import { Component, OnInit } from '@angular/core';
import { DemoResponse, DummyControllerService } from 'generated/angular-client';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  data!:DemoResponse;
  constructor(private dummyService:DummyControllerService) {
    this.dummyService = dummyService;
  }
  ngOnInit(): void {
    this.dummyService.returnAppName().subscribe(data=> this.data= data);
  }
}
