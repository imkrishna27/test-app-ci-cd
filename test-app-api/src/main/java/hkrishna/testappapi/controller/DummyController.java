package hkrishna.testappapi.controller;

import hkrishna.testappapi.model.DemoResponse;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class DummyController {

    @GetMapping("/v1")
    @ApiResponse(
                    responseCode = "200", description = "Demo Response",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = DemoResponse.class))
    })
    public DemoResponse returnAppName() {
        return new DemoResponse(10,"some error occurred");
    }
}
