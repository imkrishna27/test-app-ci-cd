package hkrishna.testappapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DemoResponse {
    Integer id;
    String error;

}
