package hkrishna.testappapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestAppApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestAppApiApplication.class, args);
	}

}
