package hkrishna.testappapi;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class TestAppApiApplicationTests {

	@Autowired
	Environment environment;
	@Test
	void contextLoads() {
		System.out.println(environment.getProperty("local.server.port"));
	}

}
